// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FatherChaser.generated.h"

class AGameJamCharacter;
class AGameJamGameMode;

UCLASS()
class GAMEJAM_API AFatherChaser : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFatherChaser();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		float ChaseSpeed = 0.5f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsMoving = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsTeleportSound = true;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsTouchingPlayer = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsInRadiusVision = false;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsFatherDeath = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		float TimeToTeleport = 3.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		float TeleportDistance = 750.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		float DeathViewDistance = 400.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		float DistanceToTouchPlayer = 50.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		float AnimationDeathTime = 2.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		float PlayerViewFatherTime = 2.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		FVector StartPlayerLocation;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
		USkeletalMeshComponent* SkeletalMeshComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default, meta = (AllowPrivateAccess = "true"))
		class UCapsuleComponent* CapsuleCollider;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UAudioComponent* TeleportSound;

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnCapsuleBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnCapsuleEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	FTimerHandle TeleportRateTimerHandle;
	FTimerHandle DeahtTimerHandle;
	FTimerHandle PlayerTouchedTimerHandle;
	FTimerHandle ViewingFatherTimerHandle;
	AGameJamCharacter* Player;
	AGameJamGameMode* GameMode;
	bool bViewinFatherStart = true;

	void MoveToPlayer(float DeltaTime);
	float CalculateDistance();
	void Teleport();
	void AnimDeath();
	void ReachPlayer();
	void DeathTimer();
	void EndPlayerViewFatherTime();
	bool IsCharacterLookingLeft(AActor* Char);
	bool IsCharacterFacedToMe();
	void TeleportPlayer();


};

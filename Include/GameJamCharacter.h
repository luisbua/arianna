// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/PointLightComponent.h"
#include "GameJamCharacter.generated.h"

class APickUpObject;
class ABrekeableObject;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTextDelegate);

UCLASS(config=Game)
class AGameJamCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	AGameJamCharacter();

	bool bIsTouchgWeapon = false;
	bool IsInteractObject = false;
	bool bIsTouchingBrekeable = false;
	ABrekeableObject* BreakObject;
	APickUpObject* PickupObject;

	/** Side view camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/*UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Default, meta = (AllowPrivateAccess = "true"))
		class UPointLightComponent* LightComponent;*/

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Default, meta = (AllowPrivateAccess = "true"))
		class UBoxComponent* BoxCollider;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Default, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* WeaponPlace;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		TSubclassOf<APickUpObject> WeaponClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UAudioComponent* Hand_Wood_Sound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UAudioComponent* Hand_Rock_Sound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UAudioComponent* Hand_Metal_Sound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UAudioComponent* Hammer_Wood_Sound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UAudioComponent* Hammer_Rock_Sound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UAudioComponent* Hammer_Metal_Sound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UAudioComponent* Wood_Broken_Sound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UAudioComponent* Rock_Broken_Sound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UAudioComponent* Metal_Broken_Sound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UAudioComponent* JumpUpSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UAudioComponent* LandedSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UAudioComponent* ReleaseSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UAudioComponent* PickUpSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsCarryingWeapon = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		int32 NumWaysEnded = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsTeleportSound = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsVisibleText = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsPickUpSound = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsReleaseSound = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsHandWoodNoBreakSound = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsHandWoodBreakSound = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsHammerRockNoBreakSound = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsHammerRockBreakSound = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		bool bIsLastWallSound = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UMaterialInstance* MaterialBreak;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		bool bIsReachLastWall = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float JumpImpulse = 750.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float CameraLength = 1500.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		bool bIsJump = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		bool bIsLandedSound = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		bool bIsJumpSound = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float NumOfJumps = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float LightRadius = 10.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		float AmountToIcrease = 4.0f;

	UPROPERTY()
		int32 DoubleJumpCounter;

	UPROPERTY(BlueprintAssignable, Category = "NDG Events")
		FTextDelegate OnTextDelegate;


	UFUNCTION()
		void DoubleJump();

	void IncreaseLightRadius();

	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }




protected:

	virtual void BeginPlay() override;

	UFUNCTION()
		void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	/** Called for side to side input */
	void MoveRight(float Val);

	/** Handle touch inputs. */
	void TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location);

	/** Handle touch stop event. */
	void TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

	virtual void Landed(const FHitResult& Hit) override;

private:

	void CharacterJump();
	void HitObject();
	bool FaceToObject(AActor* Object);
	void EndGame();
	
	UFUNCTION(BlueprintCallable)
		void InteractObject();
};
	

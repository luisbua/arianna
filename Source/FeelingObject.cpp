	// Fill out your copyright notice in the Description page of Project Settings.


#include "FeelingObject.h"
#include "GameJamCharacter.h"


// Sets default values
AFeelingObject::AFeelingObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	RootComponent = MeshComp;

}

// Called when the game starts or when spawned
void AFeelingObject::BeginPlay()
{
	Super::BeginPlay();

	MeshComp->OnComponentHit.AddDynamic(this, &AFeelingObject::OnHit);
}

void AFeelingObject::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	UE_LOG(LogTemp, Warning, TEXT("He tocado el objeto Feeling"));
	AGameJamCharacter* Character = Cast<AGameJamCharacter>(OtherActor);
	if (Character)
	{
		Character->IncreaseLightRadius();
		Destroy();
	}
}

// Called every frame
void AFeelingObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


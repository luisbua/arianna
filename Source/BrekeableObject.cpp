	// Fill out your copyright notice in the Description page of Project Settings.


#include "BrekeableObject.h"
#include "GameJamCharacter.h"

// Sets default values
ABrekeableObject::ABrekeableObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	RootComponent = SceneComp;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	MeshComp->SetupAttachment(SceneComp);
}

// Called when the game starts or when spawned
void ABrekeableObject::BeginPlay()
{
	Super::BeginPlay();
	
	//MeshComp->OnComponentHit.AddDynamic(this, &ABrekeableObject::OnHit);
	MeshComp->SetMaterial(0, MaterialNormal);
}

// Called every frame
void ABrekeableObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABrekeableObject::Hitwall_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("Ejecutando Hit Wall desde C++"));
}

void ABrekeableObject::SaidText_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("Ejecutando Said Text desde C++"));
}

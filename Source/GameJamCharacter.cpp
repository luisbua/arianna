// Copyright Epic Games, Inc. All Rights Reserved.

#include "GameJamCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/BoxComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "PickUpObject.h"
#include "BrekeableObject.h"
#include "Kismet/GameplayStatics.h"
#include "GameJamGameMode.h"
#include "Components/AudioComponent.h"

AGameJamCharacter::AGameJamCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(35.f, 91.0f);
	GetMesh()->SetRelativeLocation(FVector(0.0f, 0.0f, -91.0f));

	// Don't rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Rotation of the character should not affect rotation of boom
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->TargetArmLength = CameraLength;
	CameraBoom->SocketOffset = FVector(0.f,0.f,75.f);
	CameraBoom->SetRelativeRotation(FRotator(0.f,180.f,0.f));

	////Create a Light Component attach to root
	//LightComponent = CreateDefaultSubobject<UPointLightComponent>(TEXT("PointLight"));
	//LightComponent->SetupAttachment(RootComponent);
	//LightComponent->SetAttenuationRadius(LightRadius);
	//LightComponent->SetRelativeLocation(FVector(100.0f, 0.0f, 0.0f));

	BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("Fist Trigger"));
	BoxCollider->SetupAttachment(RootComponent);
	BoxCollider->SetRelativeLocation(FVector(70.0f, 0.0f, 0.0f));
	BoxCollider->SetRelativeScale3D(FVector(0.50f, 1.0f, 2.5f));

	//Create a WeaponPlace attach to root
	WeaponPlace = CreateDefaultSubobject<USceneComponent>(TEXT("Weapon Place"));
	WeaponPlace->SetupAttachment(RootComponent);
	WeaponPlace->SetRelativeLocation(FVector(100.0f, 0.0f, 0.0f));

	//Create Sounds
	Hand_Wood_Sound = CreateDefaultSubobject<UAudioComponent>(TEXT("Hand Wood Sound"));
	Hand_Wood_Sound->bAutoActivate = false;
	Hand_Rock_Sound = CreateDefaultSubobject<UAudioComponent>(TEXT("Hand Rock Sound"));
	Hand_Rock_Sound->bAutoActivate = false;
	Hand_Metal_Sound = CreateDefaultSubobject<UAudioComponent>(TEXT("Hand Metal Sound"));
	Hand_Metal_Sound->bAutoActivate = false;
	Hammer_Wood_Sound = CreateDefaultSubobject<UAudioComponent>(TEXT("Hammer Wood Sound"));
	Hammer_Wood_Sound->bAutoActivate = false;
	Hammer_Rock_Sound = CreateDefaultSubobject<UAudioComponent>(TEXT("Hammer Rock Sound"));
	Hammer_Rock_Sound->bAutoActivate = false;
	Hammer_Metal_Sound = CreateDefaultSubobject<UAudioComponent>(TEXT("Hammer Metal Sound"));
	Hammer_Metal_Sound->bAutoActivate = false;
	Wood_Broken_Sound = CreateDefaultSubobject<UAudioComponent>(TEXT("Wood Broken Sound"));
	Wood_Broken_Sound->bAutoActivate = false;
	Rock_Broken_Sound = CreateDefaultSubobject<UAudioComponent>(TEXT("Rock Broken Sound"));
	Rock_Broken_Sound->bAutoActivate = false;
	Metal_Broken_Sound = CreateDefaultSubobject<UAudioComponent>(TEXT("Metal Broken Sound"));
	Metal_Broken_Sound->bAutoActivate = false;
	JumpUpSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Jump Up"));
	JumpUpSound->bAutoActivate = false;
	LandedSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Landed"));
	LandedSound->bAutoActivate = false;
	ReleaseSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Release Hammer"));
	ReleaseSound->bAutoActivate = false;
	PickUpSound = CreateDefaultSubobject<UAudioComponent>(TEXT("PickUp Hammer"));
	PickUpSound->bAutoActivate = false;
	

	// Create a camera and attach to boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->bUsePawnControlRotation = false; // We don't want the controller rotating the camera

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->GravityScale = 2.f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	GetCharacterMovement()->MaxFlySpeed = 600.f;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

}

void AGameJamCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	BoxCollider->OnComponentBeginOverlap.AddDynamic(this, &AGameJamCharacter::OnBoxBeginOverlap);
	BoxCollider->OnComponentEndOverlap.AddDynamic(this, &AGameJamCharacter::OnBoxEndOverlap);

	
}

void AGameJamCharacter::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	ABrekeableObject* BO = Cast<ABrekeableObject>(OtherActor);
	if (BO)
	{
		bIsTouchingBrekeable = true;
		BreakObject = BO;
	}
	else
	{
		APickUpObject* PO = Cast<APickUpObject>(OtherActor);
		if (PO)
		{
			bIsTouchgWeapon = true;
			PickupObject = PO;
		}
	}
	UE_LOG(LogTemp, Warning, (TEXT("Begin Overlap Character")));
	
}

void AGameJamCharacter::OnBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	bIsTouchingBrekeable = false;
	bIsTouchgWeapon = false;
	UE_LOG(LogTemp, Warning, (TEXT("End Overlap Character")));
}

//////////////////////////////////////////////////////////////////////////
// Input

void AGameJamCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	PlayerInputComponent->BindAction("Hit", IE_Pressed, this, &AGameJamCharacter::HitObject);
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AGameJamCharacter::InteractObject);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AGameJamCharacter::DoubleJump);
	PlayerInputComponent->BindAction("EndGame", IE_Released, this, &AGameJamCharacter::EndGame);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAxis("MoveRight", this, &AGameJamCharacter::MoveRight);

	PlayerInputComponent->BindTouch(IE_Pressed, this, &AGameJamCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AGameJamCharacter::TouchStopped);
}

void AGameJamCharacter::Landed(const FHitResult& Hit)
{
	DoubleJumpCounter = 0;
	bIsJump = false;
	LandedSound->Play();
}

void AGameJamCharacter::DoubleJump()
{
	if (DoubleJumpCounter <= NumOfJumps - 1.0)
	{
		ACharacter::LaunchCharacter(FVector(0.0f, 0.0f, JumpImpulse), false, true);
		DoubleJumpCounter++;
		bIsJump = true;
		JumpUpSound->Play();
	}
}


void AGameJamCharacter::MoveRight(float Value)
{
	// add movement in that direction
	AddMovementInput(FVector(0.f, -1.f, 0.f), Value);
}

void AGameJamCharacter::TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	// jump on any touch
	Jump();
}

void AGameJamCharacter::TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	StopJumping();
}


void AGameJamCharacter::IncreaseLightRadius()
{
	LightRadius -= AmountToIcrease;
}


void AGameJamCharacter::CharacterJump()
{
	Jump();
}

void AGameJamCharacter::HitObject()
{
	if (bIsTouchingBrekeable && BreakObject /*&& FaceToObject(BreakObject)*/)
	{
		float Dist = FVector::Distance(BreakObject->GetActorLocation(), GetActorLocation());
		UE_LOG(LogTemp, Warning, TEXT("Distancia: %f"), Dist);
		if (Dist < 280.0f)
		{
			if (!bIsCarryingWeapon) //Hit with Hand
			{
				if (BreakObject->IsHandBrekeable) //Wooden Wall
				{
					Hand_Wood_Sound->Play();
					BreakObject->SaidText();
					BreakObject->NumHits++;
					BreakObject->MeshComp->SetMaterial(0, BreakObject->MaterialBreak);
					UE_LOG(LogTemp, Warning, TEXT("Numero de Golpes: %i"), BreakObject->NumHits);
					if (BreakObject->NumHitsToDestroy == BreakObject->NumHits)
					{
						BreakObject->bIsDestroy = true;
						BreakObject->SetActorHiddenInGame(true);
						BreakObject->MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
						AGameJamGameMode* GameMode = Cast<AGameJamGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
						GameMode->bBreakWall = true;
						Wood_Broken_Sound->Play();
					}
				}
				else //Rock Wall
				{
					if (BreakObject->IsLastWall) //Is Last Wall (METAL)
					{
						BreakObject->Hitwall();
						Hand_Metal_Sound->Play();
					}
					else //Isn't Last Wall
					{
						BreakObject->Hitwall();
						Hand_Rock_Sound->Play();
					}
				}
			}
			else //Hit with Hammer
			{
				if (BreakObject->IsHandBrekeable) //Wooden Wall
				{
					Hammer_Wood_Sound->Play();
				}
				else //Rock Wall
				{
					if (BreakObject->IsLastWall) //Is Last Wall
					{
						Hammer_Metal_Sound->Play();
						UE_LOG(LogTemp, Warning, TEXT("Sulta el Puto Martillo 2"));
						BreakObject->SaidText();
						BreakObject->NumHits++;
						if (BreakObject->NumHitsToDestroy <= BreakObject->NumHits)
						{
							OnTextDelegate.Broadcast();
							bIsReachLastWall = true;
						}
					}
					else //Isn't Last Wall
					{
						Hammer_Rock_Sound->Play();
						BreakObject->SaidText();
						BreakObject->NumHits++;
						BreakObject->MeshComp->SetMaterial(0, BreakObject->MaterialBreak);
						if (BreakObject->NumHitsToDestroy == BreakObject->NumHits)
						{
							BreakObject->bIsDestroy = true;
							BreakObject->SetActorHiddenInGame(true);
							BreakObject->MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
							AGameJamGameMode* GameMode = Cast<AGameJamGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
							GameMode->bBreakWall = true;
							bIsHammerRockBreakSound = true;
							Rock_Broken_Sound->Play();
						}
					}
				}
			}
		}
	}
}

void AGameJamCharacter::InteractObject()
{
	if (bIsTouchgWeapon && PickupObject && !bIsCarryingWeapon)
	{
		bIsCarryingWeapon = true;
		FVector TemLocation = PickupObject->GetActorLocation();
		PickUpSound->Play();
		PickupObject->Destroy();
		AGameJamGameMode* GameMode = Cast<AGameJamGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		GameMode->bTakeHammer = true;
	}
	else
	{
		if (bIsCarryingWeapon)
		{
			if (GetActorForwardVector().Y < 0)
			{
				APickUpObject* Weapon = GetWorld()->SpawnActor<APickUpObject>(WeaponClass, WeaponPlace->GetComponentLocation(), FRotator(0.0f, 0.0f, 0.0f));
			}
			else
			{
				APickUpObject* Weapon = GetWorld()->SpawnActor<APickUpObject>(WeaponClass, WeaponPlace->GetComponentLocation(), FRotator(0.0f, 0.0f, 180.0f));
			}
			if (bIsReachLastWall)
			{
				UE_LOG(LogTemp, Warning, TEXT("Soltando martillo y tocando ultimo muro"));
				BreakObject->Destroy();
			}
			ReleaseSound->Play();
			bIsCarryingWeapon = false;
			bIsTouchgWeapon = false;
		}
	}
}

bool AGameJamCharacter::FaceToObject(AActor* Object)
{
	ABrekeableObject* BreakObj = Cast<ABrekeableObject>(Object);
	if (BreakObj)
	{
		FHitResult Hit;
		FVector Start = WeaponPlace->GetComponentLocation();
		FVector End = GetActorForwardVector() * 300.0f;
		if (GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECollisionChannel::ECC_WorldDynamic))
		{
			ABrekeableObject* ObjTmp = Cast<ABrekeableObject>(Hit.GetActor());
			if (ObjTmp == BreakObj)
			{
				return true;
			}
		}
	}
	return false;
}

void AGameJamCharacter::EndGame()
{
	UKismetSystemLibrary::QuitGame(GetWorld(),UGameplayStatics::GetPlayerController(GetWorld(),0),EQuitPreference::Quit,true);
}
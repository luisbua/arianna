// Fill out your copyright notice in the Description page of Project Settings.


#include "FatherChaser.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"
#include "GameJamCharacter.h"
#include "GameJamGameMode.h"
#include "Components/AudioComponent.h"

// Sets default values
AFatherChaser::AFatherChaser()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SkeletalMeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	RootComponent = SkeletalMeshComp;

	CapsuleCollider = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule Trigger"));
	CapsuleCollider->SetupAttachment(SkeletalMeshComp);
	CapsuleCollider->SetRelativeLocation(FVector(0.0f, 5.0f, 110.0f));
	CapsuleCollider->SetCapsuleHalfHeight(100.0f);
	CapsuleCollider->SetCapsuleRadius(25.0f);

	TeleportSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Teleport Sound"));
	TeleportSound->bAutoActivate = false;

	SetActorTickEnabled(false);
	
}

// Called when the game starts or when spawned
void AFatherChaser::BeginPlay()
{
	Super::BeginPlay();

	Player = Cast<AGameJamCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	GameMode = Cast<AGameJamGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (!Player || !GameMode)
	{
		return;
	}

	CapsuleCollider->OnComponentBeginOverlap.AddDynamic(this, &AFatherChaser::OnCapsuleBeginOverlap);
	CapsuleCollider->OnComponentEndOverlap.AddDynamic(this, &AFatherChaser::OnCapsuleEndOverlap);

	GetWorld()->GetTimerManager().SetTimer(TeleportRateTimerHandle, this, &AFatherChaser::Teleport, TimeToTeleport, true, TimeToTeleport);
	SetActorTickEnabled(true);
}

void AFatherChaser::OnCapsuleBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AGameJamCharacter* Character = Cast<AGameJamCharacter>(OtherActor);
	UCapsuleComponent* Capsule = Cast<UCapsuleComponent>(OtherComp);
	
	if (Character && Capsule)
	{
		UE_LOG(LogTemp, Warning, (TEXT("Begin Overlap Father")));
		bIsTouchingPlayer = true;
		ReachPlayer();
	}
}

void AFatherChaser::OnCapsuleEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AGameJamCharacter* Character = Cast<AGameJamCharacter>(OtherActor);
	UCapsuleComponent* Capsule = Cast<UCapsuleComponent>(OtherComp);
	if (Character && Capsule)
	{
		UE_LOG(LogTemp, Warning, (TEXT("End Overlap Father")));
		bIsTouchingPlayer = false;
	}
}

// Called every frame
void AFatherChaser::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsMoving)
	{
		MoveToPlayer(DeltaTime);
	}
	
	if (bIsTouchingPlayer)
	{
		//ReachPlayer();
	}

	AnimDeath();
	//(CalculateDistance() <= DistanceToTouchPlayer) ? bIsTouchingPlayer = true : bIsTouchingPlayer = false;

}

void AFatherChaser::MoveToPlayer(float Delta)
{
	SetActorLocation(FMath::VInterpTo(GetActorLocation(), Player->GetActorLocation()-FVector(0.0f,0.0f,90.0f), Delta, ChaseSpeed));
	float Val;
	(GetActorLocation().Y - Player->GetActorLocation().Y <= 0) ? Val = 0.0f : Val = 180.0f;
	SetActorRotation(FRotator(0.0f, Val, 0.0f));
}

float AFatherChaser::CalculateDistance()
{
	return FVector::Distance(GetActorLocation(), Player->GetActorLocation());
}

void AFatherChaser::Teleport()
{
	if (bIsMoving)
	{
		FVector NewPos = GetActorLocation() * FVector(1.0f, 0.0f, 1.0f);
		(IsCharacterLookingLeft(Player))// Always faced to plaer
		//(FMath::Rand() % 1 == 0) //Random Mode (face or back)
			? NewPos.Y = Player->GetActorLocation().Y - TeleportDistance
			: NewPos.Y = Player->GetActorLocation().Y + TeleportDistance;
	
		this->SetActorLocation(NewPos);
		TeleportSound->Play();
	}

}

void AFatherChaser::AnimDeath()
{
	(CalculateDistance() < DeathViewDistance) ? bIsInRadiusVision = true : bIsInRadiusVision = false;
	
	//UE_LOG(LogTemp, Error, TEXT("Bucle"));
	if (bIsInRadiusVision && IsCharacterFacedToMe())
	{
		bIsMoving = false;
		GetWorld()->GetTimerManager().SetTimer(DeahtTimerHandle, this, &AFatherChaser::DeathTimer, 1.0f, false, AnimationDeathTime);
		if(bViewinFatherStart) GetWorld()->GetTimerManager().SetTimer(ViewingFatherTimerHandle, this, &AFatherChaser::EndPlayerViewFatherTime, 1.0f, false, PlayerViewFatherTime);
		bViewinFatherStart = false;
		//UE_LOG(LogTemp, Warning, TEXT("Face OFF"));
	}
	else
	{
		GetWorld()->GetTimerManager().ClearTimer(ViewingFatherTimerHandle);
		bViewinFatherStart = true;
	}
}

void AFatherChaser::ReachPlayer()
{
	if (bIsTouchingPlayer)
	{
		UE_LOG(LogTemp, Warning, TEXT("Reach Player"));
		bIsFatherDeath = true;
		bIsMoving = false;
		AGameJamCharacter* Character = Cast<AGameJamCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		if (Character)
		{
			Character->DisableInput(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		}
		GetWorld()->GetTimerManager().SetTimer(PlayerTouchedTimerHandle, this, &AFatherChaser::TeleportPlayer, 1.0f, false, AnimationDeathTime);
		bIsTouchingPlayer = false;
	}
}

void AFatherChaser::DeathTimer()
{
	bIsMoving = true;
	UE_LOG(LogTemp, Warning, TEXT("Death Timer"));
}

void AFatherChaser::EndPlayerViewFatherTime()
{
	GameMode->bLevelOver = true;
	bIsMoving = false;
	bIsFatherDeath = true;
	UE_LOG(LogTemp, Error, TEXT("End Player View Father Time"));

}

bool AFatherChaser::IsCharacterLookingLeft(AActor* Char)
{
	float PlayerDirection = Char->GetActorForwardVector().Y;
	bool Direction;
	(PlayerDirection < 0.0f) ? Direction = true : Direction = false;
	
	return Direction;
}

bool AFatherChaser::IsCharacterFacedToMe()
{
	bool Faced;
	(IsCharacterLookingLeft(Player) == IsCharacterLookingLeft(this)) ? Faced = false : Faced = true;

	return Faced;
}

void AFatherChaser::TeleportPlayer()
{
	AGameJamCharacter* Character = Cast<AGameJamCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (Character)
	{
		Character->SetActorLocation(StartPlayerLocation);
		Character->EnableInput(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	}
}

